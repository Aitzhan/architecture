using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class TypeAndLevel
{
    [SerializeField] private TypeName typeName;
    [SerializeField] private GameObject prefab;
    [SerializeField] private int level;

    public int Level { get { return level; } }

    public TypeName TypeName { get { return typeName; } }

    public GameObject Prefab { get { return prefab; } }
}
[Serializable]
public class Type
{
    [SerializeField] private List<TypeAndLevel> typeAndLevel;

    public List<TypeAndLevel> TypeAndLevel
    {
        get { return typeAndLevel; }
    }
}
