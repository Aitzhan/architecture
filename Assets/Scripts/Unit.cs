using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    [SerializeField] private static AllTypes allTypes;

    public static void LoadData()
    {
        allTypes = Resources.Load<AllTypes>("Data");
    }

    /// <summary>
    /// �������� ����������� ������� ������.
    /// </summary>
    /// <param name="level">������� �������</param>
    /// <param name="typeName">���</param>
    /// <param name="spawnPos">��������</param>
    /// <returns>True, ���� ������ �� ������������� ������ � ��� ����������, ����� - False.</returns>
    public static bool Upgrade(int level, TypeName typeName, Vector3 spawnPos)
    {
        for (int i = 0; i < allTypes.Type.Count; i++)
        {
            if (allTypes.Type[i].TypeAndLevel.Count > level)
            {
                if (allTypes.Type[i].TypeAndLevel[level].TypeName == typeName)
                {
                    
                    UnityEngine.Object.Instantiate(allTypes.Type[i].TypeAndLevel[level].Prefab, spawnPos, Quaternion.identity);
                    return true;                    
                }
            }
            else
            {
                return false;
            }
        }
        return false;

       
    }
  
}
