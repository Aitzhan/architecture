using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "All Types", order = 1)]
public class AllTypes:ScriptableObject
{
    [SerializeField] private List<Type> type;

    public List<Type> Type { get { return type; } }
   
}
