using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Counter : MonoBehaviour
{
    private int _count = 0;
    public void IncreaseCounts()
    {
        this._count++;
    }

    public int GetCollisionCounts()
    {
        return _count;
    }
}
