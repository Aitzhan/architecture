using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Object : MonoBehaviour
{
    [SerializeField] private TypeName typeName;
    [SerializeField] private int level;

    public TypeName TypeName { get { return typeName; } }
    public int Level { get { return level; } }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Object>())
        {
            if (typeName == collision.gameObject.GetComponent<Object>().TypeName)
            {
                if (level == collision.gameObject.GetComponent<Object>().Level)
                {

                    gameObject.GetComponent<Counter>().IncreaseCounts();
                    collision.gameObject.GetComponent<Counter>().IncreaseCounts();

                    if (collision.gameObject.GetComponent<Counter>().GetCollisionCounts() == Constants.MAX_COUNT_COLLISION)
                    {
                        if (Unit.Upgrade(level, typeName, transform.position))
                        {
                            Destroy(gameObject);
                            Destroy(collision.gameObject);
                        }
                    }
                }
                else
                {
                    print("�� ��� �������");
                }
            }
            else
            {
                print("�� ��� ���");
            }
        }

    }
}
